#pragma once
#include "Color.h"
#include <GL/glut.h>

/**
*@brief represents point with getters and draw method
*/
class Point
{
public:
	Point(float x = 0, float y = 0, Color color= Color(255, 255, 255));
	Point(const Point& point); //copy constructor

	/**
	*@brief draws point on the screen using its x,y coordinates
	*/
	void draw();

	float getX() const;
	float getY() const;
	Color getColor() const;
	~Point();
private:
	float x, y;
	Color color;
};

